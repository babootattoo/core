package data.processing;

import core.server.model.SerializableJson;
import core.server.model.User;
import core.server.model.UserDAO;
import core.server.model.ViewModel;
import core.server.network.Server;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 *
 * @author Brice.Boutamdja
 */
public class ProcessDataConnectWindow {
    
    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;
    
    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip ip of the client
     */
    public ProcessDataConnectWindow(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ",View : "+ input.getString("View") +")");
    }
    
    /**
     * Do a certain process depending on the Action JsonValue of the input
     * JsonObject
     *
     * @return output jsonObject which will be sent to the client
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException, IllegalAccessException, InvocationTargetException,NoSuchMethodException {
        switch (input.getString("Action")) {
            case "Connection":
                builder.add("Action", input.getString("Action"));
                handlingConnection();
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output = builder.build();
        ViewModel.println("(" + ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }
    
    /**
     * Method which handle the action "Connection" It gets a User object within
     * the UserDAO with the corresponding Login and Password If the returned
     * User object is null, no user has been found on the database
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    private void handlingConnection() throws SQLException, IOException, ClassNotFoundException, InterruptedException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        User userLogged = UserDAO.getUserByLoginPassword(input.getString("Login"), input.getString("Password"));
        if (userLogged != null) {
            builder.add("State", "Success");
            builder.add("User", SerializableJson.serialize(userLogged));
            Server.cListUsers.addUser(new User(userLogged.getId(), userLogged.getLogin(), ipClient));
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No user found with this login and password");
        }
    }
    
}
