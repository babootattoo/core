CREATE TABLE TypeEmployee(
	id serial PRIMARY KEY,
	workingTypeUser varchar(255)
);

CREATE TABLE Employee(
	id serial PRIMARY KEY,
	idTypeEmployee int REFERENCES typeemployee(id),
	firstName varchar(255),
	lastName varchar(255),
	address varchar(255),
	town varchar(255),
	postalcode varchar(255),
	login varchar(255) UNIQUE,
	password varchar(255),
	email varchar(255),
	hiringDate date,
	incomePerHour decimal(5,2)
);

CREATE TABLE CardState (
	id serial PRIMARY KEY,
	description varchar(255)
);

CREATE TABLE UrgencyDegree (
	id serial PRIMARY KEY,
	description varchar(255)
);

CREATE TABLE Parking(
	id serial PRIMARY KEY,
	placeNumber varchar(10),
	occupied boolean,
	bikePlace boolean
);

CREATE TABLE RFIDChip(
	id serial PRIMARY KEY,
	tagId varchar(255),
	installationDate date
);

CREATE TABLE Bike(
	id serial PRIMARY KEY,
	idRFIDChip int REFERENCES rfidchip(id),
	purchaseDate date,
	lastRevision date
);

CREATE TABLE CarModel(
	id serial PRIMARY KEY,
	builder varchar(255),
	bodyType varchar(255),
	electric boolean,
	descriptionCarModel varchar(255)
);

CREATE TABLE NFCCase(
	id serial PRIMARY KEY,
	tagId varchar(255),
	installationDate date
);

CREATE TABLE Car(
	id serial PRIMARY KEY,
	idNFCCase int REFERENCES nfccase(id),
	idCarModel int REFERENCES carmodel(id),
	matriculation varchar(255),
	purchaseDate date,
	lastRevision date
);

CREATE TABLE ReparationForm (
	id serial PRIMARY KEY,
	idCardState int REFERENCES cardstate(id),
	idUrgencyDegree int REFERENCES urgencydegree(id),
	idCar int REFERENCES car(id),
	idBike int REFERENCES bike(id),
	idParking int REFERENCES parking(id),
	entryDate date,
	outDate date,
	diagnosis varchar(255)
);

CREATE TABLE CommentaryReparationForm (
	idReparationForm int REFERENCES reparationform(id),
	idEmployee int REFERENCES employee(id),
	creation_Date date,
	commentary varchar(255)
);

CREATE TABLE WorkOn(
	idEmployee int REFERENCES employee(id),
	idReparationForm int REFERENCES reparationform(id),
	CONSTRAINT pk_workon PRIMARY KEY (idEmployee, idReparationForm)
);

CREATE TABLE Part(
	id serial PRIMARY KEY,
	stock int,
	description varchar(255),
	bikePart boolean,
	purchasePrice decimal(5,2)
);

CREATE TABLE canUsePartCarModel (
	idCarModel int REFERENCES carmodel(id),
	idPart int REFERENCES part(id),
	CONSTRAINT pk_canusepartcarmodel PRIMARY KEY (idCarModel, idPart)
);

CREATE TABLE usePartCar (
	idCar int REFERENCES car(id),
	idPart int REFERENCES part(id),
	installationDate date,
	CONSTRAINT pk_usepartcar PRIMARY KEY (idCar, idPart)
);

CREATE TABLE usePartBike (
	idBike int REFERENCES bike(id),
	idPart int REFERENCES part(id),
	installationDate date,
	CONSTRAINT pk_usepartbike PRIMARY KEY (idBike, idPart)
);

CREATE TABLE requestPart (
	id serial PRIMARY KEY,
	idPart int REFERENCES part(id),
	idEmployee int REFERENCES employee(id),
	requestDate date
);

CREATE TABLE orderPart (
	id serial PRIMARY KEY,
	idEmployee int REFERENCES employee(id),
	orderDate date
);

CREATE TABLE orderConcernedPart (
	idOrderPart int REFERENCES  orderpart(id),
	idPart int REFERENCES part(id),
	quantity integer,
	price decimal(5,2),
	CONSTRAINT pk_orderconcernedpart PRIMARY KEY (idOrderPart, idPart)
);

CREATE TABLE requestOut (
	id serial PRIMARY KEY,
	idCar int REFERENCES car(id),
	idBike int REFERENCES bike(id),
	idEmployee int REFERENCES employee(id),
	outDate date
);

CREATE TABLE breakdownCategory (
	id serial PRIMARY KEY,
	description varchar(255)
);

CREATE TABLE breakdown (
	id serial PRIMARY KEY,
	idBreakdownCategory int REFERENCES breakdowncategory(id),
	description varchar(255),
	canOccurBike boolean
);

CREATE TABLE canHaveBreakdownCarModel (
	idCarModel int REFERENCES carmodel(id),
	idBreakdown int REFERENCES breakdown(id),
	CONSTRAINT pk_canhavebdcm PRIMARY KEY (idCarModel, idBreakdown)
);

CREATE TABLE haveBreakdownBike (
	idBike int REFERENCES bike(id),
	idBreakdown int REFERENCES breakdown(id),
	breakdownDate date,
	CONSTRAINT pk_havebdbike PRIMARY KEY (idBike, idBreakdown)
);

CREATE TABLE haveBreakdownCar (
	idCar int REFERENCES car(id),
	idBreakdown int REFERENCES breakdown(id),
	breakdownDate date,
	CONSTRAINT pk_havebdcar PRIMARY KEY (idCar, idBreakdown)
);
