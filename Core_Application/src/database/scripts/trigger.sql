/* pgcrypto needs to be added by a superuser, connect into root then postgres */
CREATE EXTENSION pgcrypto;

CREATE OR REPLACE FUNCTION crypt_password_employee() RETURNS TRIGGER AS $$
	BEGIN
		SELECT encode(digest(NEW.password, 'sha256'), 'hex') INTO NEW.password;
		RETURN NEW;
	END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER trig_cryptpwd_employee BEFORE INSERT ON employee
	FOR EACH ROW
	EXECUTE PROCEDURE crypt_password_employee();
	
/*Search for more trigger or constraints*/;