package core.client.network;

import static core.client.view.Main.Config;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * 
 * @author Stephane.Schenkel
 */
public class Communication {
    
    /**
     * declaration of attribute
     * @param s Socket
     * @param pw PrintWriter
     * @param in DataInputStream
     */
    private Socket s;
    private PrintWriter pw;
    private DataInputStream in;
    
    /**
     * This is the constructor of communication,
     * and it call initializeSocket method
     * @throws UnknownHostException
     * @throws IOException 
     */
    public Communication() throws UnknownHostException, IOException{
        this.initializeSocket();
    }
    
    /**
     * This method initialize attribut of communication
     * @throws UnknownHostException
     * @throws IOException 
     */
    private void initializeSocket() throws UnknownHostException, IOException{
        s = new Socket(Config.SERVER_IP, Config.SERVER_PORT);
        pw = new PrintWriter(new DataOutputStream(s.getOutputStream()), true);
        in = new DataInputStream(s.getInputStream());
    }
    
    /**
     * This method write in attribut printWriter for send JsonObject recup on parameter
     * @param json JsonObject
     * @throws IOException 
     */
    public void sendData(JsonObject json) throws IOException{
        pw.println(json.toString());
    }
    
    /**
     * This method affect on JsonReader the data read in DataInputStream and return the json read
     * @return JsonObject with data read in attribut DataInputStream
     * @throws IOException 
     */
    public JsonObject getData() throws IOException{
        JsonReader reader = Json.createReader(in);
        return reader.readObject();
    }
    
    /**
     * This method close the socket, printWriter and dateInputStream attribut
     * @throws IOException 
     */
    public void close() throws IOException{
        pw.println("END");
        in.close();
        pw.close();
        s.close();
    }
}
