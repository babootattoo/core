package core.client.view;

import core.client.controller.ControllerScanVehicleWindow;
import core.client.model.Parking;
import core.client.model.User;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author charles_santerre
 */
public class ScanVehicleWindow extends JFrame {

    /**
     * @param panel main JPanel of window
     * @param okButton JButton with label "submit"
     * @param cancelButton JButton with label "cancel"
     * @param tagField JTextField with size 25
     * @param listParkingAvailble ArrayList composed of Parking object
     * @param combBoxParkingAvailable JComboBox composed of ComboItem object
     * @param listBikeParkingAvailable ArrayList composed of Parking object
     * @param listCarParkingAvailable Arraylist composed of Parking object
     * @param controller Object ControllerScanVehicleWindow
     * @param selectedParking Parking select in the comboBoxParkingAvailable
     * @param bGroup ButtonGroup
     * @param carRadioButton JRadioButton with label "Car"
     * @param bikeRadioButton JRadioButton with label "Bike"
     * @param userLogged Object User, employee who use the current session
     */
    JPanel panel = new JPanel();

    JButton okButton = new JButton("submit");

    JButton cancelButton = new JButton("cancel");

    JTextField tagField = new JTextField(25);

    ArrayList<Parking> listParkingAvailable = new ArrayList<Parking>();

    JComboBox<ComboItem> comboBoxParkingAvailable = new JComboBox<ComboItem>();

    ArrayList<Parking> listBikeParkingAvailable = new ArrayList<Parking>();

    ArrayList<Parking> listCarParkingAvailable = new ArrayList<Parking>();

    ControllerScanVehicleWindow controller = new ControllerScanVehicleWindow(this.getClass().getSimpleName());

    ButtonGroup bGroup = new ButtonGroup();

    JRadioButton carRadioButton = new JRadioButton("Car");

    JRadioButton bikeRadioButton = new JRadioButton("Bike");

    private User userLogged;

    /**
     * Window's constructor
     *
     * @param listParking
     * @param userLogged
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     */
    public ScanVehicleWindow(ArrayList<Parking> listParking, User userLogged) throws InvocationTargetException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException {
        this.userLogged = userLogged;
        display(listParking);

        this.setSize(500, 250);
        this.setTitle("Scan vehicle window");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                int reponse = JOptionPane.showConfirmDialog(ScanVehicleWindow.this,
                        "Do you want to exit application ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse == JOptionPane.YES_OPTION) {
                    ScanVehicleWindow.this.dispose();
                }
            }
        });
        this.setLocationRelativeTo(null);

    }

    private void display(ArrayList<Parking> listParking) throws InvocationTargetException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException {
        this.listParkingAvailable = listParking;
        BoxLayout panelLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(panelLayout);

        /**
         * Create panels
         */
        JPanel titlePanel = new JPanel(new FlowLayout());
        JPanel tagPanel = new JPanel(new FlowLayout());
        JPanel radioPanel = new JPanel(new FlowLayout());
        JPanel buttonPanel = new JPanel(new FlowLayout());
        JPanel listPanel = new JPanel(new FlowLayout());

        carRadioButton.setSelected(true);
        bGroup.add(carRadioButton);
        bGroup.add(bikeRadioButton);

        titlePanel.add(new JLabel("Enter a tag to create a new reparation form"));
        tagPanel.add(new JLabel("tag :"));
        tagPanel.add(tagField);
        radioPanel.add(carRadioButton);
        radioPanel.add(bikeRadioButton);
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        tagField.addKeyListener(new FieldAdapter());

        this.sortList();
        listPanel.add(getComboBox(this.listCarParkingAvailable));
        /**
         * Buttons Listener
         */
        carRadioButton.addActionListener(new RadioListener());
        bikeRadioButton.addActionListener(new RadioListener());
        okButton.addMouseListener(new OkButtonClick());
        cancelButton.addMouseListener(new CancelButtonClick());

        panel.add(titlePanel);
        panel.add(tagPanel);
        panel.add(buttonPanel);
        panel.add(radioPanel);
        panel.add(listPanel);
        this.add(panel);
    }

    /**
     * Return panel with comboBox.
     *
     * @param listParking
     * @return inner JPanel
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     */
    protected JPanel getComboBox(ArrayList<Parking> listParking) throws InvocationTargetException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException {
        JPanel inner = new JPanel(new FlowLayout());
        JPanel setup = new JPanel(new BorderLayout());
        JPanel comboPanel = new JPanel(new FlowLayout());

        for (Parking p : listParking) {
            comboBoxParkingAvailable.addItem(new ComboItem(p, p.getPlaceNumber()));
        }
        comboPanel.add(comboBoxParkingAvailable);
        setup.add(comboPanel, BorderLayout.CENTER);
        inner.add(setup);
        return inner;
    }

    /**
     * listParkingAvailable is divided in two other lists. One for Car and the
     * other for Bike.
     */
    private void sortList() {
        listCarParkingAvailable.clear();
        listBikeParkingAvailable.clear();
        for (Parking p : listParkingAvailable) {
            if (!p.getBikePlace()) {
                listCarParkingAvailable.add(p);
            } else {
                listBikeParkingAvailable.add(p);
            }
        }
    }

    /**
     * This method get tagField, the userLogged, and selectedParking and use the
     * controller method createReparationForm which return a String
     */
    public void CreateReparationFormMethod() {
        try {
            ComboItem comboItem = (ComboItem) comboBoxParkingAvailable.getSelectedItem();
            String answer = controller.createReparationForm(tagField.getText(), userLogged, comboItem.getValue());
            switch (answer) {
                case "success":
                    listParkingAvailable = controller.getListParkingAvailable();
                    this.sortList();
                    refreshComboBox();
                    JOptionPane.showMessageDialog(panel, "Creation succeed", "Scan completed", JOptionPane.INFORMATION_MESSAGE);
                    break;
                case "empty":
                    JOptionPane.showMessageDialog(panel, "Please scan a vehicle", "Scan error", JOptionPane.ERROR_MESSAGE);
                    break;
                case "wrong size":
                    JOptionPane.showMessageDialog(panel, controller.getErrorMessage(), "Scan error", JOptionPane.ERROR_MESSAGE);
                    break;
                case "wrong":
                    JOptionPane.showMessageDialog(panel, controller.getErrorMessage(), "Scan error", JOptionPane.ERROR_MESSAGE);
                    break;
                case "close":
                    ScanVehicleWindow.this.dispose();
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(panel, e.getMessage(), "Scan error", JOptionPane.ERROR_MESSAGE);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ScanVehicleWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ScanVehicleWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void refreshComboBox() {
        if (carRadioButton.isSelected()) {
            comboBoxParkingAvailable.removeAllItems();
            for (Parking p : listCarParkingAvailable) {
                comboBoxParkingAvailable.addItem(new ComboItem(p, p.getPlaceNumber()));
            }
        } else if (bikeRadioButton.isSelected()) {
            comboBoxParkingAvailable.removeAllItems();
            for (Parking p : listBikeParkingAvailable) {
                comboBoxParkingAvailable.addItem(new ComboItem(p, p.getPlaceNumber()));
            }
        }
    }

    class FieldAdapter extends KeyAdapter {

        @Override
        public void keyTyped(KeyEvent args) {
            if (args.getKeyChar() == KeyEvent.VK_ENTER) {
                CreateReparationFormMethod();
            }
        }
    }

    class OkButtonClick extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent args) {
            CreateReparationFormMethod();
        }
    }

    /**
     * this listener allows to switch from listCarParkingAvailable to
     * listBikeParkingAvailable or th opposite when we use radioButton
     */
    class RadioListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            refreshComboBox();
        }
    }

    class CancelButtonClick extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent args) {
            tagField.setText("");
        }
    }

    /**
     * This class allows to show the number place of each parking and keep the
     * object when we select it
     */
    class ComboItem {

        private Parking value;
        private String label;

        public ComboItem(Parking value, String label) {
            this.value = value;
            this.label = label;
        }

        public Parking getValue() {
            return this.value;
        }

        public String getLabel() {
            return this.label;
        }

        @Override
        public String toString() {
            return "Number place : " + label;
        }
    }
}
