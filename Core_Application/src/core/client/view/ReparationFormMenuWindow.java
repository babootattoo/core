package core.client.view;

import core.client.controller.ControllerReparationFormMenuWindow;
import core.client.model.Bike;
import core.client.model.Car;
import core.client.model.DateFormatted;
import core.client.model.Parking;
import core.client.model.ReparationForm;
import core.client.model.User;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

/**
 * View for reparation form menu window
 * @author brice.boutamdja
 */
public class ReparationFormMenuWindow extends JFrame{
    
    JPanel panel = new JPanel();
    
    final DefaultListModel vehicle = new DefaultListModel();
        
    private JList list = new JList(vehicle);
    
    private JTable table;
    
    private ControllerReparationFormMenuWindow controller = null;
    
    private User userLogged;
    
    private Modele modele;
   
    /**
     * 
     * @param lRepForm
     * @param userLogged
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException 
     */
    public ReparationFormMenuWindow(List<ReparationForm> lRepForm, User userLogged) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IllegalArgumentException{

        this.userLogged = userLogged;
        controller = new ControllerReparationFormMenuWindow(this.getClass().getSimpleName(), userLogged);
        /**
         * Parameters of the window
         */
        Display(lRepForm, userLogged);
      

    }
    
    /**
     * Initialize JTable
     * @param lRepForm list of reparation form
     * @param userLogged user
     */
    private void initialize(List<ReparationForm> lRepForm, User userLogged){
        JScrollPane jScrollPane1 = new javax.swing.JScrollPane();
        
        modele = new Modele(lRepForm);
        table = new JTable(modele);
        table.addMouseListener(new PopClickListener(lRepForm,userLogged));
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jScrollPane1.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
        );
    }
    
    /**
     * Modele customize for adapt table with reparation form
     */
    public class Modele extends AbstractTableModel{
        
        private List<ReparationForm> lRepForm;
        private String[] title = {"id", "descriptionCardState", "descriptionUrgencyDegree", "parkingPlace", "car", "bike", "entryDate","outDate","diagnosis"};

        
        public String getColumnName(int columnIndex){
            return title[columnIndex];
        }
        
        public Modele(List<ReparationForm> lRepForm){
            this.lRepForm = lRepForm;
        }
        
        public void setNewList(List<ReparationForm> lRepForm){
            this.lRepForm = lRepForm;
        }

        @Override
        public int getRowCount() {
            return lRepForm.size();
        }

        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex){
                case 0 :
                    return Integer.class;
                case 1 :
                    return String.class;
                case 2 :
                    return String.class;
                case 3 :
                    return Parking.class;
                case 4 :
                    return Car.class;
                case 5 :
                    return Bike.class;
                case 6 :
                    return DateFormatted.class;
                case 7 :
                    return DateFormatted.class;
                case 8 :
                    return String.class;
                default :
                    return Object.class;
            }
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex){
                case 0 :
                    return lRepForm.get(rowIndex).getId();
                case 1 :
                    return lRepForm.get(rowIndex).getDescriptionCardState();
                case 2 :
                    return lRepForm.get(rowIndex).getDescriptionUrgencyDegree();
                case 3 :
                    return lRepForm.get(rowIndex).getParkingPlace().getPlaceNumber();
                case 4 :
                    if (lRepForm.get(rowIndex).getCar() == null)
                        return "";
                    else
                        return lRepForm.get(rowIndex).getCar().getMatriculation();
                case 5 :
                    if (lRepForm.get(rowIndex).getBike() == null)
                        return "";
                    else
                        return lRepForm.get(rowIndex).getBike().getId();
                case 6 :
                    return lRepForm.get(rowIndex).getEntryDate();
                case 7 :
                    return lRepForm.get(rowIndex).getOutDate();
                case 8 :
                    return lRepForm.get(rowIndex).getDiagnosis();
                default :
                    throw new IllegalArgumentException();
            }
        }

        @Override
        public int getColumnCount() {
            return title.length;
        }
    }
    
    /**
     * class for pop up on click right
     */
    class PopUp extends JPopupMenu {
    JMenuItem anItem;
    JMenuItem SecondItem;
        public PopUp(int row, List<ReparationForm> lRepForm,User userLogged){
            anItem = new JMenuItem("Modify reparation form");
            SecondItem = new JMenuItem("Refresh");
            add(anItem);
            add(SecondItem);
            anItem.addMouseListener(new openNewWindowListener(row, lRepForm, userLogged));
            SecondItem.addMouseListener(new refreshListenner(lRepForm, userLogged));
        }
    }
    
    /**
     * class for refresh table
     */
    class refreshListenner extends MouseAdapter {

        private User userLogged;
        private List<ReparationForm> lRepForm;

        public void mousePressed(MouseEvent e){
            try {
                lRepForm = controller.getListReparationForm();
                
                table.removeAll();
                modele.setNewList(lRepForm);
                modele.fireTableDataChanged();
                
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ReparationFormMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }

        public refreshListenner(List<ReparationForm> lRepForm, User userLogged){
            this.lRepForm = lRepForm;
            this.userLogged = userLogged;
        }
    }

    /**
     * class for open window reparation form detail window
     */
    class openNewWindowListener extends MouseAdapter {
        private int row;
        private User userLogged;
        private List<ReparationForm> lRepForm;

        public void mousePressed(MouseEvent e){
            int idRepForm = Integer.valueOf(modele.getValueAt(row, 0).toString());
            controller.openReparationFormDetailWindow(lRepForm, userLogged, idRepForm);
        }

        public openNewWindowListener(int row, List<ReparationForm> lRepForm, User userLogged){
            this.row = row;
            this.lRepForm = lRepForm;
            this.userLogged = userLogged;
        }
    }
    
    /**
     * class for pop click listener
     */
    class PopClickListener extends MouseAdapter {
        private int row;
        private User userLogged;
        private List<ReparationForm> lRepForm;
        
        public PopClickListener(List<ReparationForm> lRepForm, User userLogged){
            this.userLogged = userLogged;
            this.lRepForm = lRepForm;
        }
        
        public void mousePressed(MouseEvent e){
            if (e.isPopupTrigger()){
                int lineSelected = table.getSelectedRow();
                this.row = lineSelected;
                doPop(e);
            }
        }

        /*public void mouseReleased(MouseEvent e){
            if (e.isPopupTrigger())
                doPop(e);
        }*/


        private void doPop(MouseEvent e){
            PopUp menu = new PopUp(row, lRepForm, userLogged);
            menu.show(e.getComponent(), e.getX(), e.getY());
        }
    }
    
    /**
     * Method wich contain all parameter of window 
     * @param lRepForm list of reparation form
     * @param userLogged User
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalArgumentException 
     */
    private void Display(List<ReparationForm> lRepForm, User userLogged) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, IllegalArgumentException {
        this.initialize(lRepForm, userLogged);
        this.setSize(1200,300);
        this.setTitle("Reparation Form Menu");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int reponse = JOptionPane.showConfirmDialog(ReparationFormMenuWindow.this,
                        "Do you want to exit application ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION)
                    ReparationFormMenuWindow.this.dispose();
            }
        });
        this.setLocationRelativeTo(null);
    }
    
      
}
