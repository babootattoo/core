package core.client.model;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class User {

    /**
     * Declaration of attribute
     *
     * @param id Int
     * @param typeUser String
     * @param firstName String
     * @param lastName String
     * @param address String
     * @param town String
     * @param postalCode String
     * @param login String
     * @param email String
     * @param hiringDate DateFormatted
     * @param incomingPerHour Double
     */
    private int id;
    private String typeUser;
    private String firstName;
    private String lastName;
    private String address;
    private String town;
    private String postalCode;
    private String login;
    private String email;
    private DateFormatted hiringDate;
    private Double incomePerHour;

    public User(int id, String typeuser, String firstname, String lastname, String address, String town, String postalcode, String login, String email, DateFormatted hiringDate, Double incomePerHour) {
        this.id = id;
        this.typeUser = typeuser;
        this.firstName = firstname;
        this.lastName = lastname;
        this.address = address;
        this.town = town;
        this.postalCode = postalcode;
        this.login = login;
        this.email = email;
        this.hiringDate = hiringDate;
        this.incomePerHour = incomePerHour;
    }
    
    public User(int id, String login){
        this.id = id;
        this.login = login;
    }

    @Override
    public String toString() {
        return "user{" + "id=" + id + ", typeuser=" + typeUser + ", firstname=" + firstName + ", lastname=" + lastName + ", address=" + address + ", town=" + town + ", postalcode=" + postalCode + ", login=" + login + ", email=" + email + ", hiringDate=" + hiringDate + ", incomingPerHour=" + incomePerHour + '}';
    }

    public static User deserialize(JsonObject inputJson) throws ParseException {
        if (inputJson.getString("id").equals("-1")) {
            return null;
        } else {
            return new User(Integer.parseInt(inputJson.getString("id")), inputJson.getString("typeUser"), inputJson.getString("firstName"), inputJson.getString("lastName"), inputJson.getString("address"), inputJson.getString("town"), inputJson.getString("postalCode"), inputJson.getString("login"), inputJson.getString("email"), new DateFormatted(inputJson.getString("hiringDate")), Double.parseDouble(inputJson.getString("incomePerHour")));
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

        public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeUser) {
        this.typeUser = typeUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastname) {
        this.lastName = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalcode) {
        this.postalCode = postalcode;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DateFormatted getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(DateFormatted hiringDate) {
        this.hiringDate = hiringDate;
    }

    public Double getIncomingPerHour() {
        return incomePerHour;
    }

    public void setIncomingPerHour(Double incomingPerHour) {
        this.incomePerHour = incomingPerHour;
    }
    
    public static String cryptPassword(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes("UTF-8"));
        byte[] digest = md.digest();
        String cryptedPassword = String.format("%064x", new java.math.BigInteger(1, digest));
        return cryptedPassword;
    }
}
