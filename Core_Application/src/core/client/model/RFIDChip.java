package core.client.model;

import java.text.ParseException;
import java.util.Objects;
import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class RFIDChip {
    
    private int id;
    private String tagId;
    private DateFormatted installationDate;

    public RFIDChip(int id, String tagId, DateFormatted installationDate) {
        this.id = id;
        this.tagId = tagId;
        this.installationDate = installationDate;
    }

    public int getId() {
        return id;
    }

    public String getTagId() {
        return tagId;
    }

    public DateFormatted getInstallationDate() {
        return installationDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public void setInstallationDate(DateFormatted installationDate) {
        this.installationDate = installationDate;
    }

    @Override
    public String toString() {
        return "RFIDChip{" + "id=" + id + ", tagId=" + tagId + ", installationDate=" + installationDate + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id;
        hash = 89 * hash + Objects.hashCode(this.tagId);
        hash = 89 * hash + Objects.hashCode(this.installationDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RFIDChip other = (RFIDChip) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.tagId, other.tagId)) {
            return false;
        }
        if (!Objects.equals(this.installationDate, other.installationDate)) {
            return false;
        }
        return true;
    }
    
    public static RFIDChip deserialize(JsonObject inputJson) throws ParseException{
        if(inputJson.getString("id").equals(-1)){
            return null;
        }else{
            return new RFIDChip(Integer.parseInt(inputJson.getString("id")), inputJson.getString("tagId"), new DateFormatted(inputJson.getString("installationDate")));
        }
    }
    
}
