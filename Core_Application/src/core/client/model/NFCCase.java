package core.client.model;

import java.text.ParseException;
import java.util.Objects;
import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class NFCCase {
    
    private int id;
    private String tagId;
    private DateFormatted installationDate;

    public NFCCase(int id, String tagId, DateFormatted installationDate) {
        this.id = id;
        this.tagId = tagId;
        this.installationDate = installationDate;
    }
    
    public static NFCCase deserialize(JsonObject inputJson) throws ParseException{
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            return new NFCCase(Integer.parseInt(inputJson.getString("id")), inputJson.getString("tagId"), new DateFormatted(inputJson.getString("installationDate")));
        }
    }

    public int getId() {
        return id;
    }

    public String getTagId() {
        return tagId;
    }

    public DateFormatted getInstallationDate() {
        return installationDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public void setInstallationDate(DateFormatted installationDate) {
        this.installationDate = installationDate;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + this.id;
        hash = 73 * hash + Objects.hashCode(this.tagId);
        hash = 73 * hash + Objects.hashCode(this.installationDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NFCCase other = (NFCCase) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.tagId, other.tagId)) {
            return false;
        }
        if (!Objects.equals(this.installationDate, other.installationDate)) {
            return false;
        }
        return true;
    }
    
    
    
}
