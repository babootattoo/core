package core.client.model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author Charles.Santerre
 */
public class Car{
    
    /**
     * Declaration of attribute
     * @param id Int
     * @param matriculation String
     * @param purchaseDate DateFormatted
     * @param lastRevision DateFormatted
     */
    private int id;
    private String matriculation;
    private DateFormatted purchaseDate; 
    private DateFormatted lastRevision; 
    private NFCCase nfcCase;
    private CarModel model;
    
    private List<Part> parts;
    private List<Breakdown> breakdowns;

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public void setBreakdowns(List<Breakdown> breakdowns) {
        this.breakdowns = breakdowns;
    }

    public List<Part> getParts() {
        return parts;
    }

    public List<Breakdown> getBreakdowns() {
        return breakdowns;
    }

    public Car(int id, String matriculation, DateFormatted purchaseDate, DateFormatted lastRevision, NFCCase nfcCase, CarModel model, List<Part> parts, List<Breakdown> breakdowns) {
        this.id = id;
        this.matriculation = matriculation;
        this.purchaseDate = purchaseDate;
        this.lastRevision = lastRevision;
        this.nfcCase = nfcCase;
        this.model = model;
        this.parts = parts;
        this.breakdowns = breakdowns;
    }

    public NFCCase getNfcCase() {
        return nfcCase;
    }

    public CarModel getModel() {
        return model;
    }

    public void setNfcCase(NFCCase nfcCase) {
        this.nfcCase = nfcCase;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }
    
    public static Car deserialize(JsonObject inputJson) throws ParseException{
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            NFCCase nfc = NFCCase.deserialize(inputJson.getJsonObject("nfcCase"));
            CarModel model = CarModel.deserialize(inputJson.getJsonObject("model"));
            
            JsonArray partList = inputJson.getJsonArray("parts_list");
            JsonArray breakdownList = inputJson.getJsonArray("breakdowns_list");
            List<Part> listPart = new ArrayList<Part>();
            List<Breakdown> listBreakdown = new ArrayList<Breakdown>();
            if(partList != null){
                for(int i = 0; i < partList.size(); i++){
                    listPart.add(Part.deserialize(partList.getJsonObject(i).getJsonObject("Part_"+i)));
                }
            }
            if(breakdownList != null){
                for(int i = 0; i < breakdownList.size(); i++){
                    listBreakdown.add(Breakdown.deserialize(breakdownList.getJsonObject(i).getJsonObject("Breakdown_"+i)));
                }
            }
            return new Car(Integer.parseInt(inputJson.getString("id")), inputJson.getString("matriculation"), new DateFormatted(inputJson.getString("purchaseDate")), new DateFormatted(inputJson.getString("lastRevision")), nfc, model, listPart, listBreakdown);
        }
    }

    @Override
    public String toString() {
        return "Car{" + "id=" + id + ", matriculation=" + matriculation + ", purchaseDate=" + purchaseDate + ", lastRevision=" + lastRevision + '}';
    }
   
    public int getId() {
        return id;
    }

    public String getMatriculation() {
        return matriculation;
    }

    public DateFormatted getPurchaseDate() {
        return purchaseDate;
    }

    public DateFormatted getLastRevision() {
        return lastRevision;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMatriculation(String matriculation) {
        this.matriculation = matriculation;
    }

    public void setPurchaseDate(DateFormatted purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public void setLastRevision(DateFormatted lastRevision) {
        this.lastRevision = lastRevision;
    } 
}
