package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Stephane.Schenkel
 */
public class BreakdownDAO {
    
    /**
     * This method return JsonObject for get listbreakdown depends on carmodel with JsonObjectBuilder
     * @param idCarModel
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestListBreakdownCarModel(int idCarModel, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getListBreakdownCarModel");
        builder.add("ConnectionState", "Try");
        builder.add("IdCarModel", idCarModel);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of breakdown of carModel
     * @param idCarModel
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListBreakdownCarModel(int idCarModel, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestListBreakdownCarModel(idCarModel, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject for get listbreakdown of every Bike with JsonObjectBuilder
     * @param idCarModel
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestListBreakdownBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getListBreakdownBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of breakdown of bike
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListBreakdownBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestListBreakdownBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
}
