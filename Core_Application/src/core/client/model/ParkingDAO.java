package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Stéphane.Schenkel
 */
public class ParkingDAO {
    
    /**
     * This method return JsonObject for get parking with JsonObjectBuilder
     * @param classView
     * @param userLogged
     * @return 
     */
    private static JsonObject getAvailableParkingPlaceList(String classView, User userLogged){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveListAvailableParkingList");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of available parking place
     * @param classView
     * @param userLogged
     * @return
     * @throws IOException 
     */
    public static JsonObject getListVehicleWaitingForReparation(String classView, User userLogged) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getAvailableParkingPlaceList(classView, userLogged));
        result = com.getData();
        com.close();
        return result;
    }
}
