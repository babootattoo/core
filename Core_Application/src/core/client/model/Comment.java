package core.client.model;

import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author rayde
 */
public class Comment {
    
    public String fullName;
    public DateFormatted creationDate;
    public String commentary;

    public Comment(String fullName, DateFormatted creationDate, String commentary) {
        this.fullName = fullName;
        this.creationDate = creationDate;
        this.commentary = commentary;
    }

    public String getFullName() {
        return fullName;
    }

    public DateFormatted getCreationDate() {
        return creationDate;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setCreationDate(DateFormatted creationDate) {
        this.creationDate = creationDate;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }
    
    public static Comment deserialize(JsonObject inputJson) throws ParseException{
        return new Comment(inputJson.getString("fullName"), new DateFormatted(inputJson.getString("creationDate")), inputJson.getString("commentary"));
    }
    
}
