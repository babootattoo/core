package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Charles.Santerre, Stephane.Schenkel
 */
public class PartDAO {
    
    /**
     * This method return JsonObject to get list of Part depends on carModel with JsonObjectBuilder
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestUsablePartCarModel(int idCarModel, User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getUsablePartCarModel");
        builder.add("ConnectionState", "Try");
        builder.add("IdCarModel", idCarModel);
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of usablePart
     * @param idCarModel
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListUsablePartCarModel(int idCarModel, User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestUsablePartCarModel(idCarModel, userLogged, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to get list of Part of every bike with JsonObjectBuilder
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getJsonRequestListUsablePartBike(User userLogged, String classView){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getListUsablePartBike");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json with the list of usablePart of PartBike
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getListUsablePartBike(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getJsonRequestListUsablePartBike(userLogged,classView));
        result = com.getData();
        com.close();
        return result;
    }
    
}
