package core.client.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Stephane.Schenkel, Brice.Boutamdja
 */
public class DateFormatted {

    /**
     * Declaration of attribute
     *
     * @param date Date
     * @param format DateFormat
     */
    Date date;
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    public DateFormatted(String date) throws ParseException {
        if (date.equals("null")) {
            this.date = null;
        }else{
            this.date = format.parse(date);
        }
    }

    public DateFormatted(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        if (date != null) {
            return format.format(date);
        } else {
            return null;
        }
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDate(String date) throws ParseException {
        this.date = format.parse(date);
    }

}
