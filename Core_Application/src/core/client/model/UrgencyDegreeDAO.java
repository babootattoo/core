package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Stephane.Schenkel
 */
public class UrgencyDegreeDAO {
    
    /**
     * This method build JsonObject to get all urgencyDegree with JsonObjectBuilder
     * @param classView
     * @param userLogged
     * @return 
     */
    private static JsonObject getAllUrgencyDegreeRequestJson(String classView, User userLogged){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "getAllUrgencyDegree");
        builder.add("ConnectionState", "Try");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        json = builder.build();
        return json;
    }
    
    /**
     * This method get all urgencyDegree
     * @param userLogged
     * @param classView
     * @return
     * @throws IOException 
     */
    public static JsonObject getAllUrgencyDegree(User userLogged, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getAllUrgencyDegreeRequestJson(classView, userLogged));
        result = com.getData();
        com.close();
        return result;
    }
    
}
