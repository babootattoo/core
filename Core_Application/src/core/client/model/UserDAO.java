package core.client.model;

import core.client.network.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Stephane.Schenkel
 */
public class UserDAO {
    
    /**
     * This method return JsonObject for connection with JsonObjectBuilder
     * @param login String login of User class
     * @param password String password of User class
     * @return JsonObject
     */
    private static JsonObject getLoginRequestJson(String login, String password, String classView){
        JsonObject result = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "Connection");
        builder.add("View", classView);
        builder.add("ConnectionState", "Try");
        builder.add("Login", login);
        builder.add("Password", password);
        result = builder.build();
        return result;
    }
    
    /**
     * This method collect json to user give by login and password on param
     * @param login String login of User class
     * @param password String password of User class
     * @return JsonObject
     * @throws IOException 
     */
    public static JsonObject tryLogin(String login, String password, String classView) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getLoginRequestJson(login, password, classView));
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject to disconnect user with JsonObjectBuilder
     * @param userLogged
     * @param classView
     * @return 
     */
    private static JsonObject getDisconnectUserJson(User userLogged, String classView){
        JsonObject result = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "Disconnection");
        builder.add("View", classView);
        builder.add("User_id", userLogged.getId());
        builder.add("User_login", userLogged.getLogin());
        result = builder.build();
        return result;
    }
    
    /**
     * This method disconnect user
     * @param userLogged
     * @param classView
     * @throws IOException 
     */
    public static void disconnectUser(User userLogged, String classView) throws IOException{
        Communication com = new Communication();
        com.sendData(getDisconnectUserJson(userLogged, classView));
        com.close();
    }
    
}
