package core.client.controller;

import core.client.model.ReparationFormDAO;
import core.client.model.ReparationForm;
import core.client.model.User;
import core.client.view.ConnectWindow;
import core.client.view.ReparationFormDetailWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 * class controller reparation form menu window
 * @author brice.boutamdja
 */
public class ControllerReparationFormMenuWindow extends Controller{
    
    private String classView;
    private User userLogged;
    
    /**
     * Controller
     * @param classView String
     * @param userLogged User
     */
    public ControllerReparationFormMenuWindow(String classView,User userLogged){
        this.classView = classView;
        this.userLogged = userLogged;
    }
    
    /**
     * This method receive and send list of reparation form
     * @return List of reparation form
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws ParseException 
     */
    public List<ReparationForm> getListReparationForm() throws IOException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException{
        List<ReparationForm> lRepForm= new ArrayList<ReparationForm>();
        JsonObject inputJson = verifJson(ReparationFormDAO.getReparationFormAll(classView, userLogged));
        String stateCom = inputJson.getString("State");
        System.out.println(stateCom);
        switch (stateCom){
            case "Success" :
                JsonArray JArray = inputJson.getJsonArray("ListReparationForm");
                for(int i = 0; i < JArray.size(); i++){
                    JsonObject jsonRepForm = JArray.getJsonObject(i);
                    ReparationForm rf = ReparationForm.deserialize(jsonRepForm.getJsonObject("ReparationForm_"+i));
                    lRepForm.add(rf);
                }
                break;
            case "Error" :
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return lRepForm;
    }
    
    /**
     * This method open reparation form detail window
     * @param lRepForm list of reparation form
     * @param userLogged user
     * @param row integer
     */
    public void openReparationFormDetailWindow(List<ReparationForm> lRepForm, User userLogged, int idForm) {
        int i = 0;
        while(i < lRepForm.size() && lRepForm.get(i).getId() != idForm){
            i++;
        }
        if(i < lRepForm.size()){
            new ReparationFormDetailWindow(lRepForm.get(i), userLogged);
        }else{
            errorMessage = "No reparation form found in the list with this id";
        }
    }
}
