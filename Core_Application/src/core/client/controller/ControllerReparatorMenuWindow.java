package core.client.controller;

import core.client.model.Parking;
import core.client.model.ParkingDAO;
import core.client.model.ReparationForm;
import core.client.model.ReparationFormDAO;
import core.client.model.User;
import core.client.view.ConnectWindow;
import core.client.view.ReparationFormMenuWindow;
import core.client.view.ScanVehicleWindow;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author charles.santerre
 */
public class ControllerReparatorMenuWindow extends Controller{
    
    private String classView;
    private User userLogged;
    public ControllerReparatorMenuWindow(String classView, User userLogged) {
        this.classView = classView;
        this.userLogged = userLogged;
    }
    
    public String openScanVehicleWindow() throws IOException, InvocationTargetException, NoSuchMethodException, UnsupportedEncodingException, NoSuchAlgorithmException, ParseException, IllegalArgumentException, IllegalAccessException, NoSuchAlgorithmException{
        String answer = "";
        ArrayList<Parking> listParking = new ArrayList<Parking>();
        JsonObject inputJson = verifJson(ParkingDAO.getListVehicleWaitingForReparation(classView, userLogged));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray JArray = inputJson.getJsonArray("Parking_list");
                for(int i = 0; i < JArray.size(); i++){
                    JsonObject jsonParking = JArray.getJsonObject(i);
                    Parking park = Parking.deserialize(jsonParking.getJsonObject("Parking_"+i));
                    listParking.add(park);
                }
                new ScanVehicleWindow(listParking, userLogged);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                answer = "close";
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return answer;
    }
    
    public String openReparationForm() throws IOException, NoSuchAlgorithmException, ParseException, IllegalAccessException, InvocationTargetException, IllegalArgumentException, NoSuchMethodException{
        String answer = "";
        List<ReparationForm> lRepForm= new ArrayList<ReparationForm>();
        JsonObject inputJson = verifJson(ReparationFormDAO.getReparationFormAll(classView, userLogged));
        String stateCom = inputJson.getString("State");
        System.out.println(stateCom);
        switch (stateCom){
            case "Success" :
                JsonArray JArray = inputJson.getJsonArray("ListReparationForm");
                for(int i = 0; i < JArray.size(); i++){
                    JsonObject jsonRepForm = JArray.getJsonObject(i);
                    ReparationForm rf = ReparationForm.deserialize(jsonRepForm.getJsonObject("ReparationForm_"+i));
                    lRepForm.add(rf);
                }
                new ReparationFormMenuWindow(lRepForm, userLogged);
                break;
            case "Error" :
                answer = "error";
                errorMessage = inputJson.getString("ErrorMessage");
                break;
            case "Cancelled_Reconnection":
                answer = "close";
                new ConnectWindow();
                break;
            case "Reconnection_Success":
                userLogged = User.deserialize(inputJson.getJsonObject("User"));
                break;
        }
        return answer;
    }
    
    public void disconnectUser() throws IOException{
        super.disconnectUser(userLogged, classView);
    }
}
