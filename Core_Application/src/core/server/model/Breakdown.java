package core.server.model;

import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class Breakdown {
    
    private int id;
    private String category;
    private String description;
    
    private DateFormatted occuredDate;

    public Breakdown(int id, String category, String description, DateFormatted occuredDate) {
        this.id = id;
        this.category = category;
        this.description = description;
        this.occuredDate = occuredDate;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public DateFormatted getOccuredDate() {
        return occuredDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOccuredDate(DateFormatted occuredDate) {
        this.occuredDate = occuredDate;
    }
    
    public static Breakdown deserialize(JsonObject inputJson) throws ParseException{
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            return new Breakdown(Integer.parseInt(inputJson.getString("id")), inputJson.getString("category"), inputJson.getString("description"), new DateFormatted(inputJson.getString("occuredDate")));
        }
    }
    
}
