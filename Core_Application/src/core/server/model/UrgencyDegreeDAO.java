package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Stephane.Schenkel
 */
public class UrgencyDegreeDAO {
    
    /**
     * Get the list of all urgency degree of the database
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<String> getAllString() throws ClassNotFoundException, SQLException, InterruptedException{
        List<String> result = new ArrayList<String>();
        
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT description FROM urgencydegree ORDER BY id asc");
        ResultSet rs = stat.executeQuery();
        
        while(rs.next()){
            result.add(rs.getString("description"));
        }
        
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        
        return result;
    }
    
}
