package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * All database methods related to the Parking class
 * @author Stephane.Schenkel
 */
public class ParkingDAO {
    
    /**
     * Create a list of Parking object by searching  all parking which have occupied at false
     * @return result of Car object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     * @throws java.text.ParseException 
     */
    public static ArrayList<Parking> getListAvailableParking() throws SQLException, ClassNotFoundException, InterruptedException, ParseException{
        ArrayList<Parking> result = new ArrayList<Parking>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT id, placeNumber, occupied, bikePlace "
                + "FROM parking p "
                + "WHERE occupied = false");
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(new Parking(rs.getInt("id"), rs.getString("placeNumber"), rs.getBoolean("occupied"), rs.getBoolean("bikePlace")));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    public static int setParkingPlaceOccupiedTrue(int idParking) throws ClassNotFoundException, SQLException, InterruptedException{
       Connection co = Datasource.getConnection();
       PreparedStatement stat = co.prepareStatement("UPDATE parking SET occupied = true WHERE id = ?");
       stat.setInt(1, idParking);
       int answer = stat.executeUpdate();
       stat.close();
       Datasource.returnConnection(co);
       return answer;
    }
}
