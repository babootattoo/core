package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Stephane.Schenkel
 */
public class PartDAO {
    
    /**
     * Get all the part used by the specified vehicle
     * @param idVehicle
     * @param table
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<Part> getUsedListByVehicleId(int idVehicle, String table) throws ClassNotFoundException, SQLException, InterruptedException{
        List<Part> result = new ArrayList<Part>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT p.id as id, stock, description, purchaseprice, installationdate FROM part p JOIN usepart"+ table +" upc on p.id = upc.idpart WHERE id"+table+" = ?");
        stat.setInt(1, idVehicle);
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(new Part(rs.getInt("id"), rs.getInt("stock"), rs.getString("description"), rs.getDouble("purchaseprice"), new DateFormatted(rs.getDate("installationdate"))));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Get all the available part for bikes
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static List<Part> getAvailablePartBike() throws SQLException, ClassNotFoundException, InterruptedException{
        List<Part> result = new ArrayList<Part>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT id, stock, description, purchaseprice FROM part WHERE bikepart = true");
        ResultSet rs = stat.executeQuery();
        
        Date datenull = null;
        while(rs.next()){
            result.add(new Part(rs.getInt("id"), rs.getInt("stock"), rs.getString("description"), rs.getDouble("purchaseprice"), new DateFormatted(datenull)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Get all the available part for the specified carmodel
     * @param idModel
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static List<Part> getAvailablePartModel(int idModel) throws SQLException, ClassNotFoundException, InterruptedException{
        List<Part> result = new ArrayList<Part>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT p.id as id, stock, description, purchaseprice FROM part p JOIN canusepartcarmodel cp ON p.id = cp.idpart WHERE idcarmodel = ?");
        stat.setInt(1, idModel);
        ResultSet rs = stat.executeQuery();
        
        Date datenull = null;
        while(rs.next()){
            result.add(new Part(rs.getInt("id"), rs.getInt("stock"), rs.getString("description"), rs.getDouble("purchaseprice"), new DateFormatted(datenull)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
