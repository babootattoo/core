package core.server.model;

import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author Charles.Santerre
 */
public class Part{
    
    /**
     *
     * Declaration of attribute
     * 
     * @param id Int
     * @param stock Int
     * @param description String
     * @param purchasePrice Double
     */
    private int id;
    private int stock;
    private String description;
    private Double purchasePrice;
    
    private DateFormatted installationDate;

    public Part(int id, int stock, String description, Double purchasePrice, DateFormatted installationDate) {
        this.id = id;
        this.stock = stock;
        this.description = description;
        this.purchasePrice = purchasePrice;
        this.installationDate = installationDate;
    }

    public void setInstallationDate(DateFormatted installationDate) {
        this.installationDate = installationDate;
    }

    public DateFormatted getInstallationDate() {
        return installationDate;
    }
    
    public static Part deserialize(JsonObject inputJson) throws ParseException{
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            return new Part(Integer.parseInt(inputJson.getString("id")), Integer.parseInt(inputJson.getString("stock")), inputJson.getString("description"), Double.parseDouble(inputJson.getString("purchasePrice")), new DateFormatted(inputJson.getString("installationDate")));
        }
    }

    @Override
    public String toString() {
        return "Part{" + "id=" + id + ", stock=" + stock + ", description="+ description +", purchasePrice="+ purchasePrice +"}";
    }

    public int getId() {
        return id;
    }

    public int getStock() {
        return stock;
    }

    public String getDescription() {
        return description;
    }

    public Double getPurchasePrice() {
        return purchasePrice;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
}
