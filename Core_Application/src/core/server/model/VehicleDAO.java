package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Stephane.Schenkel
 */
public class VehicleDAO {
    
    /**
     * Delete the part for the specified vehicle
     * @param idVehicle
     * @param idPart
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int deletePart(int idVehicle, int idPart, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("DELETE FROM usepart"+table+" WHERE id"+table+" = ? AND idpart = ?");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idPart);
        
        int result = stat.executeUpdate();
        
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Add the part for the specified vehicle
     * @param idVehicle
     * @param idPart
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int addPart(int idVehicle, int idPart, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("INSERT INTO usepart"+table+" VALUES (?, ?, now())");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idPart);
        
        int result = stat.executeUpdate();
        
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Delete the breakdown for the specified vehicle
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int deleteBreakdown(int idVehicle, int idBreakdown, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("DELETE FROM havebreakdown"+table+" WHERE id"+table+" = ? AND idbreakdown = ?");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idBreakdown);
        
        int result = stat.executeUpdate();
        
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Add the breakdown for the specified vehicle
     * @param idVehicle
     * @param idBreakdown
     * @param table
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static int addBreakdown(int idVehicle, int idBreakdown, String table) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("INSERT INTO havebreakdown"+table+" VALUES (?, ?, now())");
        stat.setInt(1, idVehicle);
        stat.setInt(2, idBreakdown);
        
        int result = stat.executeUpdate();
        
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
