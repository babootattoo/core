package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * All database methods related to the Car class
 * @author Stephane.Schenkel, Brice.Boutamdja, Charles.Santerre
 */
public class CarDAO {
    
    /**
     * Create a car object by searching a car from the car table of the database with the specified id
     * @param id int Object
     * @return car object or null object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static Car getCarById(int id) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        Car result = null;
        PreparedStatement stat = co.prepareStatement("SELECT idnfccase, idCarModel, matriculation, purchasedate, lastrevision "
                + "FROM car "
                + "WHERE id = ?");
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        Datasource.returnConnection(co);
        if (rs.next()) {
            List<Part> parts = PartDAO.getUsedListByVehicleId(id, "car");
            List<Breakdown> breakdowns = BreakdownDAO.getHaveListByVehicleId(id, "car");
            NFCCase nfccase = NFCCaseDAO.getCaseById(rs.getInt("idnfccase"));
            CarModel model = CarModelDAO.getModelById(rs.getInt("idCarModel")) ;
            result = new Car(id, rs.getString("matriculation"), new DateFormatted(rs.getDate("purchasedate")), new DateFormatted(rs.getDate("lastrevision")), nfccase, model, parts, breakdowns);
        }
        rs.close();
        stat.close();
        return result;
    }
    
}
