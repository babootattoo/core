package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 * All database methods related to the Bike class
 * @author Stephane.Schenkel
 */
public class BikeDAO {
    
    /**
     * Create a bike object by searching a bike from the bike table of the database with the specified id
     * @param id
     * @return a bike object or a null object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static Bike getBikeById(int id) throws SQLException, ClassNotFoundException, InterruptedException, ParseException{
        Connection co = Datasource.getConnection();
        Bike result = null;
        PreparedStatement stat = co.prepareStatement("SELECT idrfidchip, purchasedate, lastrevision "
                + "FROM bike "
                + "WHERE id = ?");
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        Datasource.returnConnection(co);
        if (rs.next()) {
            List<Part> parts = PartDAO.getUsedListByVehicleId(id, "bike");
            List<Breakdown> breakdowns = BreakdownDAO.getHaveListByVehicleId(id, "bike");
            RFIDChip chip = RFIDChipDAO.getChipById(rs.getInt("idrfidchip"));
            result = new Bike(id, new DateFormatted(rs.getDate("purchasedate")), new DateFormatted(rs.getDate("lastrevision")), chip, parts, breakdowns);
        }
        rs.close();
        stat.close();
        return result;
    }
    
}
