package core.server.model;

import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author Stephane.Schenkel
 */
public class ReparationForm{

    /**
     *
     * Declaration of attribute
     *
     * @param id Int
     * @param descriptionCardState String
     * @param descriptionUrgencyDegree String
     * @param parkingPlace Object Parking
     * @param car Object Car
     * @param bike Object Bike
     * @param entryDate Date
     * @param outDate Date
     * @param diagnosis String
     * @param reparationDetail String
     * @param listPartUsed ArrayList of Part
     */
    private int id;
    private String descriptionCardState;
    private String descriptionUrgencyDegree;
    private Parking parkingPlace;
    private Car car;
    private Bike bike;
    private DateFormatted entryDate;
    private DateFormatted outDate;
    private String diagnosis;

    public ReparationForm(int id, String descriptionCardState, String descriptionUrgencyDegree, Parking parkingPlace, Car car, Bike bike, DateFormatted entryDate, DateFormatted outDate, String diagnosis) {
        this.id = id;
        this.descriptionCardState = descriptionCardState;
        this.descriptionUrgencyDegree = descriptionUrgencyDegree;
        this.parkingPlace = parkingPlace;
        this.car = car;
        this.bike = bike;
        this.entryDate = entryDate;
        this.outDate = outDate;
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "ReparationForm{" + "id=" + id + ", descriptionCardState=" + descriptionCardState + ", descriptionUrgencyDegree=" + descriptionUrgencyDegree + ", parkingPlace=" + parkingPlace.toString() + ", car=" + ((car != null) ? car.toString() : "null") + ", bike=" + ((bike != null) ? bike.toString() : "null") + ", entryDate=" + entryDate + ", outDate=" + outDate + ", diagnosis=" + diagnosis + "}";
    }

    public static ReparationForm deserialize(JsonObject inputJson) throws ParseException {
        if (inputJson.getString("id").equals("-1")) {
            return null;
        } else {
            System.out.println(inputJson.toString());
            Car car = Car.deserialize(inputJson.getJsonObject("car"));
            Bike bike = Bike.deserialize(inputJson.getJsonObject("bike"));
            Parking parking = Parking.deserialize(inputJson.getJsonObject("parkingPlace"));
            return new ReparationForm(Integer.parseInt(inputJson.getString("id")), inputJson.getString("descriptionCardState"), inputJson.getString("descriptionUrgencyDegree"), parking, car, bike, new DateFormatted(inputJson.getString("entryDate")), new DateFormatted(inputJson.getString("outDate")), inputJson.getString("diagnosis"));
        }
    }

    public int getId() {
        return id;
    }

    public String getDescriptionCardState() {
        return descriptionCardState;
    }

    public String getDescriptionUrgencyDegree() {
        return descriptionUrgencyDegree;
    }

    public Parking getParkingPlace() {
        return parkingPlace;
    }

    public Car getCar() {
        return car;
    }

    public Bike getBike() {
        return bike;
    }

    public DateFormatted getEntryDate() {
        return entryDate;
    }

    public DateFormatted getOutDate() {
        return outDate;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescriptionCardState(String descriptionCardState) {
        this.descriptionCardState = descriptionCardState;
    }

    public void setDescriptionUrgencyDegree(String descriptionUrgencyDegree) {
        this.descriptionUrgencyDegree = descriptionUrgencyDegree;
    }

    public void setParkingPlace(Parking parkingPlace) {
        this.parkingPlace = parkingPlace;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public void setEntryDate(DateFormatted entryDate) {
        this.entryDate = entryDate;
    }

    public void setOutDate(DateFormatted outDate) {
        this.outDate = outDate;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}
