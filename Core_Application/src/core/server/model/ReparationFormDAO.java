package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * All database methods related to the ReparationForm class
 * @author Stephane.Schenkel, Brice.Boutamdja, Charles.Santerre
 */
public class ReparationFormDAO {

    /**
     * Get all ReparationForm from ReparationForm table of the database It
     * creates the Car object or Bike object of the ReparationForm and the
     * Parking Object
     * @return list of reparation form
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException
     * @throws ParseException 
     */
    public static List<ReparationForm> getReparationFormAll() throws ClassNotFoundException, SQLException, InterruptedException, ParseException {
        Connection co = Datasource.getConnection();
        List<ReparationForm> result = new ArrayList<ReparationForm>();
        PreparedStatement stat = co.prepareStatement("SELECT rf.id as idRepForm, rf.idcar, rf.idbike,  rf.entrydate, rf.outdate, rf.diagnosis, cs.description as descriptionCardState, "
                + "ud.description as descriptionUrgencyDegree, pk.id as idParking, pk.placenumber as placeNumberParking, pk.occupied, pk.bikePlace"
                + " FROM reparationform rf, urgencyDegree ud, cardState cs, parking pk"
                + " WHERE rf.idCardState = cs.id AND rf.idUrgencyDegree = ud.id AND rf.idparking = pk.id AND cs.description = 'Waiting for reparation'");

        ResultSet rs = stat.executeQuery();
        Datasource.returnConnection(co);
        while (rs.next()) {
            Car car = CarDAO.getCarById(rs.getInt("idcar"));
            Bike bike = BikeDAO.getBikeById(rs.getInt("idbike"));
            Parking parking = new Parking(rs.getInt("idParking"), rs.getString("placeNumberParking"), rs.getBoolean("occupied"), rs.getBoolean("bikePlace"));
            ReparationForm rf = new ReparationForm(Integer.parseInt(rs.getString("idRepForm")), rs.getString("descriptionCardState"), rs.getString("descriptionUrgencyDegree"), parking, car, bike, new DateFormatted(rs.getDate("entrydate")), new DateFormatted(rs.getDate("outdate")), rs.getString("diagnosis"));
            result.add(rf);
        }
        rs.close();
        stat.close();
        return result;
    }

    /**
     * delete a repearationForm by idCar from ReparationForm table of the
     * database
     *
     * @param car
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException
     */
    public static void deleteRepForm(Car car) throws ClassNotFoundException, SQLException, InterruptedException {
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("DELETE FROM reparationForm WHERE idCar = " + car.getId());
        stat.executeUpdate();
        stat.close();
        Datasource.returnConnection(co);
    }

    /**
     * Create ReparationForm a new reparationForm with the new car
     *
     * @param tag
     * @param idParking
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static int createReparationForm(String tag, int idParking) throws SQLException, ClassNotFoundException, InterruptedException {
        int answerParking = ParkingDAO.setParkingPlaceOccupiedTrue(idParking);
        int answer = 0;
        if (answerParking == 1) {
            Connection co = Datasource.getConnection();
            PreparedStatement stat = null;
            switch (tag.length()) {
                case 8:
                    stat = co.prepareStatement(
                            "INSERT INTO ReparationForm(idCardState, idUrgencyDegree, idParking, idCar, idBike, entryDate) VALUES"
                            + " (2,1,?,"
                            + " (SELECT c.id FROM car c JOIN nfcCase nfc ON nfc.id = c.idnfccase"
                            + " WHERE nfc.tagid = ?),"
                            + " null,(SELECT current_date))");
                    stat.setInt(1, idParking);
                    stat.setString(2, tag);
                    break;
                case 12:
                    stat = co.prepareStatement(
                            "INSERT INTO ReparationForm(idCardState, idUrgencyDegree, idParking, idCar, idBike, entryDate) VALUES"
                            + " (2,1,?,null,"
                            + " (SELECT b.id FROM bike b JOIN rfidChip rfid ON rfid.id = b.idrfidchip"
                            + " WHERE rfid.tagid = ?),"
                            + " (SELECT current_date))");
                    stat.setInt(1, idParking);
                    stat.setString(2, tag);
                    break;
            }
            answer = stat.executeUpdate();
            stat.close();
            Datasource.returnConnection(co);
        }
        return answer;
    }

    /**
     * Update the reparationform given in the parameter
     * @param repForm
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static int updateReparationForm(ReparationForm repForm) throws ClassNotFoundException, SQLException, InterruptedException {
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("UPDATE reparationform SET idcardstate = (SELECT id FROM cardstate WHERE description LIKE ?)"
                + ", idurgencydegree = (SELECT id FROM urgencydegree WHERE description LIKE ?)"
                + ", entrydate = ?"
                + ", outdate = ?"
                + ", diagnosis = ?"
                + " WHERE id = ?");
        stat.setString(1, repForm.getDescriptionCardState());
        stat.setString(2, repForm.getDescriptionUrgencyDegree());
        if (repForm.getEntryDate().toString() == null) {
            stat.setDate(3, null);
        } else {
            stat.setDate(3, java.sql.Date.valueOf(repForm.getEntryDate().toString()));
        }
        if (repForm.getOutDate().toString() == null) {
            stat.setDate(4, null);
        } else {
            stat.setDate(4, java.sql.Date.valueOf(repForm.getOutDate().toString()));
        }
        stat.setString(5, repForm.getDiagnosis());
        stat.setInt(6, repForm.getId());

        int result = stat.executeUpdate();

        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
}
