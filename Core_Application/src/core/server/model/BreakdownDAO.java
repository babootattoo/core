package core.server.model;

import database.connection.pool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Stéphane.Schenkel
 */
public class BreakdownDAO {
    
    /**
     * Return the list of breakdown that the specified vehicle has.
     * @param idVehicle
     * @param table
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static List<Breakdown> getHaveListByVehicleId(int idVehicle, String table) throws ClassNotFoundException, SQLException, InterruptedException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("select b.id as id, b.description as bdesc, bc.description as bcdesc, breakdowndate from breakdown b join havebreakdown"+ table +" hb on b.id = hb.idbreakdown"
                + " JOIN breakdowncategory bc ON b.idbreakdowncategory = bc.id WHERE id"+table+" = ?");
        stat.setInt(1, idVehicle);
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(new Breakdown(rs.getInt("id"), rs.getString("bcdesc"), rs.getString("bdesc"), new DateFormatted(rs.getDate("breakdowndate"))));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Return the list of available breakdown by the bike.
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static List<Breakdown> getAvailableBreakdownBike() throws SQLException, ClassNotFoundException, InterruptedException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT b.id as id, b.description as bdesc, bc.description as bcdesc FROM breakdown b JOIN breakdowncategory bc ON b.idbreakdowncategory = bc.id WHERE canoccurbike = true");
        ResultSet rs = stat.executeQuery();
        
        Date datenull = null;
        while(rs.next()){
            result.add(new Breakdown(rs.getInt("id"), rs.getString("bcdesc"), rs.getString("bdesc"), new DateFormatted(datenull)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Return the list of breakdown available by the specified carmodel
     * @param idModel
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static List<Breakdown> getAvailableBreakdownModel(int idModel) throws SQLException, ClassNotFoundException, InterruptedException{
        List<Breakdown> result = new ArrayList<Breakdown>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT b.id as id, b.description as bdesc, bc.description as bcdesc FROM breakdown b JOIN breakdowncategory bc ON b.idbreakdowncategory = bc.id JOIN canhavebreakdowncarmodel chb ON b.id = chb.idbreakdown WHERE idcarmodel = ?");
        stat.setInt(1, idModel);
        ResultSet rs = stat.executeQuery();
        
        Date datenull = null;
        while(rs.next()){
            result.add(new Breakdown(rs.getInt("id"), rs.getString("bcdesc"), rs.getString("bdesc"), new DateFormatted(datenull)));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
