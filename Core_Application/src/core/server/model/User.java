package core.server.model;

import java.text.ParseException;
import javax.json.JsonObject;

/**
 * User class corresponding to the user table of the database
 * @author Stephane.Schenkel, Charles.Santerre, Brice.Boutamdja
 */
public class User {
    
    private int id;
    private String typeUser;
    private String firstName;
    private String lastName;
    private String address;
    private String town;
    private String postalCode;
    private String login;
    private String email;
    private DateFormatted hiringDate;
    private Double incomePerHour;
    
    private String lastIP;
    private DateFormatted lastAccessDate;

    public User(int id, String typeuser, String firstname, String lastname, String address, String town, String postalcode, String login, String email, DateFormatted hiringDate, double incomePerHour) {
        this.id = id;
        this.typeUser = typeuser;
        this.firstName = firstname;
        this.lastName = lastname;
        this.address = address;
        this.town = town;
        this.postalCode = postalcode;
        this.login = login;
        this.email = email;
        this.hiringDate = hiringDate;
        this.incomePerHour = incomePerHour;
        this.lastIP = null;
        this.lastAccessDate = null;
    }
    
    public User(int id, String login, String lastIP){
        this.id = id;
        this.login = login;
        this.lastIP = lastIP;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", typeUser=" + typeUser + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", town=" + town + ", postalCode=" + postalCode + ", login=" + login + ", email=" + email + ", hiringDate=" + hiringDate + ", incomePerHour=" + incomePerHour + ", lastIP=" + lastIP + ", lastAccessDate=" + lastAccessDate + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(String typeuser) {
        this.typeUser = typeuser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastname) {
        this.lastName = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalcode) {
        this.postalCode = postalcode;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DateFormatted getHiringDate() {
        return hiringDate;
    }

    public void setHiringDate(DateFormatted hiringDate) {
        this.hiringDate = hiringDate;
    }

    public double getIncomePerHour() {
        return incomePerHour;
    }

    public void setIncomePerHour(Double incomePerHour) {
        this.incomePerHour = incomePerHour;
    }
    
    public void setLastIP(String lastIP) {
        this.lastIP = lastIP;
    }

    public void setLastAccessDate(DateFormatted lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    public String getLastIP() {
        return lastIP;
    }

    public DateFormatted getLastAccessDate() {
        return lastAccessDate;
    }
        
    public static User deserialize(JsonObject inputJson) throws ParseException {
        if (inputJson.getString("id").equals("-1")) {
            return null;
        } else {
            return new User(Integer.parseInt(inputJson.getString("id")), inputJson.getString("typeUser"), inputJson.getString("firstName"), inputJson.getString("lasstName"), inputJson.getString("address"), inputJson.getString("town"), inputJson.getString("postalCode"), inputJson.getString("login"), inputJson.getString("email"), new DateFormatted(inputJson.getString("hiringDate")), Double.parseDouble(inputJson.getString("incomePerHour")));
        }
    }
}
