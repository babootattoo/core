package core.server.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;

/**
 * This class contains all the static method to display a string into the console
 * with a specified format : "(Thread-Numero) [HH:MM:SS] : String"
 * @author Stephane.Schenkel
 */
public class ViewModel {
    
    private static Calendar cal;
    private static String dir = System.getProperty("user.dir") + "/logs";
    
    /**
     * Write the string into the corresponding file which is a log file
     * This log file has the actual date for name.
     * @param logString 
     */
    private static void writeLog(String logString){
        try{
            File createdir = new File(dir);
            if(!createdir.exists()){
                createdir.mkdir();
            }
            String fileName = cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH)+1 + "-" + cal.get(Calendar.DAY_OF_MONTH) + ".log";
            BufferedWriter bw = new BufferedWriter(new FileWriter(dir + "/" + fileName, true));
            bw.write(logString + "\n");
            bw.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Display the string in parameter into the console
     * @param string 
     */
    public static void println(String string){
        cal = Calendar.getInstance();
        cal.setTime(new Date());
        String time = "[" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND) + ":" + cal.get(Calendar.MILLISECOND) + "]";
        String threadName = "(" + Thread.currentThread().getName() + ")";
        System.out.println(threadName + " " + time + " " + string);
        writeLog(threadName + " " + time + " " + string);
    }
    
    /**
     * Display the stack trace of the Exception in parameter into the console
     * @param e 
     */
    public static void printError(Exception e){
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        println(errors.toString());
    }
}
