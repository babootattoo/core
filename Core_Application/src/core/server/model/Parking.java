package core.server.model;

import javax.json.JsonObject;

/**
 * Parking class corresponding to the parking table of the database
 * @author Stephane.Schenkel, Charles.Santerre, Brice.Boutamdja
 */
public class Parking {
    private int id;
    private String placeNumber;
    private boolean occupied;
    private boolean bikePlace;

    public Parking(int id, String placeNumber,boolean occupied, boolean bikePlace) {
        this.id = id;
        this.placeNumber = placeNumber;
        this.occupied = occupied;
        this.bikePlace = bikePlace;
    }

    public String getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(String placeNumber) {
        this.placeNumber = placeNumber;
    }

    public boolean getBikePlace() {
        return bikePlace;
    }

    public void setBikePlace(boolean bikePLace) {
        this.bikePlace = bikePLace;
    }
    
    
    
    @Override
    public String toString() {
        return "Parking{" + "id=" + id + ", placeNumber=" + placeNumber + ", occupied=" + occupied + ", bikePLace=" + bikePlace + '}';
    }

    public Parking(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public boolean getOccupied() {
        return occupied;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    
    public static Parking deserialize(JsonObject inputJson){
        if(inputJson.getString("id").equals("-1")){
            return null;
        }else{
            return new Parking(Integer.parseInt(inputJson.getString("id")),inputJson.getString("placeNumber"), Boolean.parseBoolean(inputJson.getString("occupied")), Boolean.parseBoolean(inputJson.getString("bikePlace")));
        }
    }
    
}
