package core.server.network;

import core.server.model.DateFormatted;
import core.server.model.User;
import core.server.model.ViewModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Stephane.Schenkel
 */
public class ConnectedUsers {
    
    /**
     * List of active users
     */
    private List<User> listUser;
    
    /**
     * Key : Id_User ; Value : Thread timeout
     */
    private Map<Integer, TimeOutThread> listThread;
    
    public ConnectedUsers(){
        listUser = new ArrayList<User>();
        listThread = new HashMap<Integer, TimeOutThread>();
    }
    
    /**
     * Check if the user if in the list of active users.
     * If he is in, it will reset the timeout thread and update the last access date.
     * If he is not in, it will return a boolean true which will indicate that the client needs to reconnect.
     * @param user
     * @return 
     */
    public boolean checkList(User user){
        boolean askConnection = false;
        int i = checkIfUserInList(user);
        if(i != -1){
            if(listThread.containsKey(user.getId())){
                if(listThread.get(user.getId()) != null){
                    listThread.get(user.getId()).interrupt();
                    listUser.get(i).setLastAccessDate(new DateFormatted(new Date()));
                }
            }
        }else{
            askConnection = true;
        }
        return askConnection;
    }
    
    /**
     * Search the specified user in the list of active users.
     * Return the index in the list if he is in, return -1 if not.
     * @param user
     * @return 
     */
    private int checkIfUserInList(User user){
        int i = 0;
        boolean found = false;
        while(i < listUser.size() && found != true){
            User usS = listUser.get(i);
            if(usS.getId() == user.getId() && usS.getLastIP().equals(user.getLastIP()) && usS.getLogin().equals(user.getLogin())){
                found = true;
            }
        }
        if(found){
            return i;
        }
        else{
            return -1;
        }
    }
    
    /**
     * Add the user in the list of active users.
     * Add a timeout thread in the map with the user id as the key.
     * @param user 
     */
    public void addUser(User user){
        int idUser = checkIfUserInList(user);
        if(idUser != -1){
            listUser.remove(idUser);
        }
        user.setLastAccessDate(new DateFormatted(new Date()));
        listUser.add(user);
        int userId = user.getId();
        TimeOutThread t = new TimeOutThread(userId);
        listThread.put(userId, t);
        listThread.get(userId).start();
    }
    
    /**
     * Call the time out thread disconnect method
     * @param user 
     */
    public void disconnection(User user){
        if(listThread.containsKey(user.getId())){
            listThread.get(user.getId()).disconnect();
        }
    }
    
    /**
     * Remove the user of the specified id of the active user list and the map
     * @param id 
     */
    public void remove(int id){
        if(listThread.containsKey(id)){
            listThread.remove(id);
        }
        int i = 0;
        while(i < listUser.size() && id != listUser.get(i).getId()){
            i++;
        }
        if(i < listUser.size()){
            listUser.remove(i);
        }
    }
    
    /**
     * Time out thread.
     * The run method is a sleep of 30minutes.
     * If the thread is interrupted the sleep will reset.
     * If it is interrupted by the disconnect method, the thread will stop and remove the user from the active user list.
     */
    class TimeOutThread extends Thread{
        boolean timeExpired = false;
        private int id;
        
        public TimeOutThread(int id){
            this.id = id;
        }
        
        public void run(){
            while(!timeExpired){
                try{
                    Thread.sleep(1000*60*30);
                    timeExpired = true;
                }catch(InterruptedException e){}
            }
            Server.cListUsers.remove(id);
        }
        
        public void disconnect(){
            timeExpired = true;
            this.interrupt();
        }
    }
    
    /**
     * Print the list of active users
     */
    public void showList(){
        String str = "";
        if(listUser.isEmpty()){
            str += "No active users.";
        }else{
            for(User user : listUser){
                str += user.getLogin() + " : last access : " + user.getLastAccessDate().toStringHour() + "\n";
            }
        }
        ViewModel.println(str);
    }
}
